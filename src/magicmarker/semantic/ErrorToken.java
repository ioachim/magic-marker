package magicmarker.semantic;

class ErrorToken {

    final int line;
    final int column;
    final String error;

    public ErrorToken(int line, int column, String error) {
        this.line = line;
        this.column = column;
        this.error = error;
    }

    @Override
    public boolean equals(Object obj) {

        if(!(obj instanceof ErrorToken)){
            return false;
        }

        ErrorToken other = (ErrorToken)obj;

        return this.error.equals(other.error);
    }

    @Override
    public String toString() {
        return String.format("%s:%s %s", line, column, error);
    }
}