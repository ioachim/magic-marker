package magicmarker.semantic;

import magicmarker.Differ;
import java.util.*;
import java.util.regex.*;
import java.io.*;


class ErrorCheck {

    static final Pattern errorRe = Pattern.compile("(?:(?:\\w+/?)*\\.cl:)?(\\d+):(\\d+)\\s*(.*)\\s*");

    List<ErrorToken> _expected;
    List<ErrorToken> _output;

    public static void main(String[] args) {

        if(args.length < 1) {
            System.out.println("Usage java -cp magicmarker.jar magicmarker.semantic.ErrorCheck <folder>");
            return;

        }
        File root = new File(args[0]);

        File[] sources = root.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".cl");
            }
        });


        double accumulatedMark = 0;
        int accumulatedSucceses = 0;

        for( File f : sources ) {
            System.err.println(f);
            try {
                ErrorCheck c = new ErrorCheck(f);
                double mark = c.process();
                accumulatedMark += mark;
                accumulatedSucceses += (int)mark;

            }
            catch(IOException e) {
                System.err.printf("Could not process file %s\n", f.getPath());
            }
        }

        double testCoverage = accumulatedSucceses/(double)sources.length;
        double successRatio = accumulatedMark/sources.length;

        System.err.printf("Test coverage: %d/%d = %.2f\n", accumulatedSucceses, sources.length, testCoverage);
        System.err.printf("Line coverage: %.2f%%", successRatio*100);
    }

    public ErrorCheck(File f) throws IOException {
        File expected = new File(f.getPath() + ".semerror");

        if( !expected.exists() ){
            System.err.printf(String.format("Could not find expected results file for %s\n", f));
            return;
        }

        ProcessBuilder pb = new ProcessBuilder("ant", String.format("-Dcool.source=%s", f.getPath()), "semantic");
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }

        String line;
        StringBuilder antOutput = new StringBuilder();
        BufferedReader out = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while((line = out.readLine()) != null) {
            antOutput.append(line);
            antOutput.append("\n");
        }

        if( p.exitValue() != 0) {
            System.out.print(antOutput);
            System.exit(-1);
        }



        File output = new File(f.getPath() + ".out");

        if( !output.exists()){
            System.err.printf(String.format("Could not find output results file for %s\n", f));
            return;
        }

        BufferedReader reader = new BufferedReader(new FileReader(expected));
        _expected = readResults(reader);
        reader.close();

        reader = new BufferedReader(new FileReader(output));
        _output = readResults(reader);
        reader.close();
    }

    public double process() throws IOException {

        assert _expected != null: "Expected is null";
        assert _output != null: "Output is null";

        assert _expected.size() > 0 : "Expected list is empty";

        Differ<ErrorToken> diff = new Differ<ErrorToken>(_expected, _output);
        diff.printDiff();

        int matched = diff.length();
        int total = Math.max(_expected.size(), _output.size());
        
        System.err.printf("%d/%d -> %d%%\n", matched, total, 100*matched/total);
        return matched/(double)total;
    }

    private List<ErrorToken> readResults(BufferedReader reader) throws IOException {
        String line;
        List<ErrorToken> list = new ArrayList<ErrorToken>();

        while((line = reader.readLine()) != null) {
            line = line.replace("\t", "    ");

            Matcher m = errorRe.matcher(line);
            if( m.matches()) {

                //System.err.printf("%s-%s [%s]\n", m.group(1), m.group(2), m.group(3));
                list.add(new ErrorToken(
                    Integer.parseInt(m.group(1)), 
                    Integer.parseInt(m.group(2)),
                    m.group(3).trim())
                );
            }                        
        }


        Collections.sort(list, new Comparator<ErrorToken>() {
            public int compare(ErrorToken l, ErrorToken r) {
                if(l.line < r.line) {
                    return -1;
                } else if (r.line > l.line) {
                    return 1;
                }

                if(l.column < r.column) {
                    return -1;
                }
                else if(r.line > l.line) {
                    return 1;
                }

                return 0;
            }

        });

        return list;
    }


}
