package magicmarker.scanner;

import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ioachim
 * Date: 3/25/13
 * Time: 7:08 PM
 */
public class Corrector {

    private List<Token> expectedTokens;
    private List<Token> outputTokens;
    private HashMap<Token.Type, Integer> valuesExpected;
    private HashMap<Token.Type, Integer> valuesGot;
    private HashMap<String, Integer> tokenExpected;
    private HashMap<String, Integer> tokenGot;
    private int[][] C;

    private double valuesRatio;
    private double tokenRatio;

    public Corrector(File f) throws IOException {


        valuesExpected = new HashMap<Token.Type, Integer>();
        valuesExpected.put(Token.Type.STRING, 0);
        valuesExpected.put(Token.Type.INT, 0);
        valuesExpected.put(Token.Type.ID, 0);
        valuesExpected.put(Token.Type.BOOLEAN, 0);
        valuesExpected.put(Token.Type.TYPE, 0);

        valuesGot = new HashMap<Token.Type, Integer>();
        valuesGot.put(Token.Type.STRING, 0);
        valuesGot.put(Token.Type.INT, 0);
        valuesGot.put(Token.Type.ID, 0);
        valuesGot.put(Token.Type.BOOLEAN, 0);
        valuesGot.put(Token.Type.TYPE, 0);

        tokenExpected = new HashMap<String, Integer>();
        tokenExpected.put("+"     , 0);
        tokenExpected.put("-"     , 0);
        tokenExpected.put("*"     , 0);
        tokenExpected.put("/"     , 0);
        tokenExpected.put("~"     , 0);
        tokenExpected.put("not"   , 0);
        tokenExpected.put("<"     , 0);
        tokenExpected.put("<="    , 0);
        tokenExpected.put("="     , 0);
        tokenExpected.put("@"     , 0);
        tokenExpected.put("=>"    , 0);
        tokenExpected.put("("     , 0);
        tokenExpected.put(")"     , 0);
        tokenExpected.put("{"     , 0);
        tokenExpected.put("}"     , 0);
        tokenExpected.put(":"     , 0);
        tokenExpected.put(";"     , 0);
        tokenExpected.put("."     , 0);
        tokenExpected.put(","     , 0);
        tokenExpected.put("<-"    , 0);
        tokenExpected.put("class"   , 0);
        tokenExpected.put("else"    , 0);
        tokenExpected.put("fi"      , 0);
        tokenExpected.put("if"      , 0);
        tokenExpected.put("in"      , 0);
        tokenExpected.put("inherits", 0);
        tokenExpected.put("isvoid"  , 0);
        tokenExpected.put("let"     , 0);
        tokenExpected.put("loop"    , 0);
        tokenExpected.put("pool"    , 0);
        tokenExpected.put("then"    , 0);
        tokenExpected.put("while"   , 0);
        tokenExpected.put("case"    , 0);
        tokenExpected.put("esac"    , 0);
        tokenExpected.put("new"     , 0);
        tokenExpected.put("of"      , 0);
        tokenExpected.put("not"     , 0);


        tokenGot = new HashMap<String, Integer>(tokenExpected);
        tokenGot.put("+"     , 0);
        tokenGot.put("-"     , 0);
        tokenGot.put("*"     , 0);
        tokenGot.put("/"     , 0);
        tokenGot.put("~"     , 0);
        tokenGot.put("not"   , 0);
        tokenGot.put("<"     , 0);
        tokenGot.put("<="    , 0);
        tokenGot.put("="     , 0);
        tokenGot.put("@"     , 0);
        tokenGot.put("=>"    , 0);
        tokenGot.put("("     , 0);
        tokenGot.put(")"     , 0);
        tokenGot.put("{"     , 0);
        tokenGot.put("}"     , 0);
        tokenGot.put(":"     , 0);
        tokenGot.put(";"     , 0);
        tokenGot.put("."     , 0);
        tokenGot.put(","     , 0);
        tokenGot.put("<-"    , 0);
        tokenGot.put("class"   , 0);
        tokenGot.put("else"    , 0);
        tokenGot.put("fi"      , 0);
        tokenGot.put("if"      , 0);
        tokenGot.put("in"      , 0);
        tokenGot.put("inherits", 0);
        tokenGot.put("isvoid"  , 0);
        tokenGot.put("let"     , 0);
        tokenGot.put("loop"    , 0);
        tokenGot.put("pool"    , 0);
        tokenGot.put("then"    , 0);
        tokenGot.put("while"   , 0);
        tokenGot.put("case"    , 0);
        tokenGot.put("esac"    , 0);
        tokenGot.put("new"     , 0);
        tokenGot.put("of"      , 0);
        tokenGot.put("not"     , 0);

        File expected = new File(f.getPath() + ".scan");

        if( !expected.exists() ){
            System.err.printf(String.format("Could not find expected results file for %s\n", f));
            return;
        }

        ProcessBuilder pb = new ProcessBuilder("ant", String.format("-Dcool.source=%s", f.getPath()), "scan");
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return;
        }

        String line;
        BufferedReader out = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while((line = out.readLine()) != null) {
            System.out.println(line);
        }

        File output = new File(f.getPath() + ".out");

        if( !output.exists()){
            System.err.printf(String.format("Could not find output results file for %s\n", f));
            return;
        }

        BufferedReader reader = new BufferedReader(new FileReader(expected));
        expectedTokens = readResults(reader);
        reader.close();

        reader = new BufferedReader(new FileReader(output));
        outputTokens = readResults(reader);
        reader.close();
    }

    public double getValuesRatio() {
        return valuesRatio;
    }

    public double getTokenRatio() {
        return tokenRatio;
    }

    public static void main(String[] args) {

        if(args.length < 1) {
            System.out.println("Usage java -cp magicmarker.jar magicmarker.scanner.Corrector <folder>");
            return;

        }
        File root = new File(args[0]);

        File[] sources = root.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".cl");
            }
        });


        double tokp = 0;
        double valp = 0;

        for( File f : sources ) {
            System.err.println(f);
            try {
                Corrector c = new Corrector(f);
                c.process();

                tokp += c.getTokenRatio();
                valp += c.getValuesRatio();
            }
            catch(IOException e) {
                System.err.printf("Could not process file %s\n", f.getPath());
            }
        }

        tokp /= sources.length;
        valp /= sources.length;

        System.out.printf("Puntaje final: 1 + 4*%f + 2*%f = %f", tokp, valp, 1+4*tokp+2*valp);

    }

    private List<Token> readResults(BufferedReader reader) throws IOException {
        String line;
        List<Token> list = new ArrayList<Token>();
        while((line = reader.readLine()) != null) {
            line = line.trim();
            if( line.equals("")) {
                continue;
            }

            try {
                list.add(Token.parse(line));

            }
            catch(Exception e) {
                System.err.printf("Could not parse line: %s", line);
            }
        }

        return list;
    }

    public void process() {

        for(Token ex : expectedTokens) {
            if(ex.getType() == Token.Type.TOKEN) {
                tokenExpected.put(ex.getValue(), tokenExpected.get(ex.getValue()) + 1);
            }
            else {
                valuesExpected.put(ex.getType(), valuesExpected.get(ex.getType()) + 1);
            }
        }

        LCSLength();
        printDiff(expectedTokens.size()-1, outputTokens.size()-1);


        int tokenExpectSum = 0, tokenSum = 0;
        int valuesExpectSum = 0, valuesSum = 0;

        for(Map.Entry<String, Integer> e: tokenExpected.entrySet()) {
            int expect = e.getValue();
            double got =tokenGot.get(e.getKey());



            if( expect > 0) {
                tokenExpectSum++;
                tokenSum += got > 0 ? got/expect : 0;
            }

            System.out.printf("%s : %d/%d\n", e.getKey(), (int)got, e.getValue());
        }

        tokenRatio = tokenExpectSum > 0 ? ((double)tokenSum)/tokenExpectSum : 0;

        for(Map.Entry<Token.Type, Integer> e: valuesExpected.entrySet()) {

            int expect = e.getValue();

            double got = valuesGot.get(e.getKey());

            if( expect > 0 ) {
                valuesExpectSum++;
                valuesSum += got > 0 ? got/expect : 0;
            }

            System.out.printf("%s : %d/%d\n", e.getKey(), valuesGot.get(e.getKey()), e.getValue());
        }

        valuesRatio = valuesExpectSum > 0 ? ((double)valuesSum)/valuesExpectSum : 0;


        System.out.printf("TOTAL: %f + %f\n", tokenRatio, valuesRatio);



    }



    public void LCSLength() {
        C = new int[expectedTokens.size()][outputTokens.size()];

        for ( int i = 0; i < expectedTokens.size(); i++) {
            C[i][0] = 0;
        }

        for ( int j = 0; j < outputTokens.size(); j++) {
                C[0][j] = 0;
        }

        for ( int i = 1; i < expectedTokens.size(); i++) {
            for ( int j = 1; j < outputTokens.size(); j++) {

                if( expectedTokens.get(i).equals(outputTokens.get(j)) ){

                    C[i][j] = C[i-1][j-1] + 1;
                }
                else {
                    C[i][j] = Math.max(C[i][j-1], C[i-1][j]);
                }
            }
        }
    }

    public void printDiff(int i, int j) {

        Token ex = expectedTokens.get(i);
        Token out = outputTokens.get(j);

        if( i > 0 && j > 0 && ex.equals(out)) {
            printDiff(i-1, j-1);

            if(ex.getType() == Token.Type.TOKEN) {
                tokenGot.put(ex.getValue(), tokenGot.get(ex.getValue()) + 1);
            }
            else {
                valuesGot.put(ex.getType(), valuesGot.get(ex.getType()) +1);
            }

            // matched both tokens
        }
        else if (j > 0 && (i == 0 || (C[i][j-1] >= C[i-1][j]))) {
            printDiff( i, j-1);
            System.err.printf("unexpected %s\n", out);

            //print "+ " + Y[j];
        }
        else if (i > 0 && (j == 0 || (C[i][j-1] < C[i-1][j]))) {
            printDiff(i-1, j);
            System.err.printf("unmatched %s\n", ex);
        }
        else if( ex.equals(out)) {
            if(ex.getType() == Token.Type.TOKEN) {
                tokenGot.put(ex.getValue(), tokenGot.get(ex.getValue()) + 1);
            }
            else {
                valuesGot.put(ex.getType(), valuesGot.get(ex.getType()) +1);
            }
        }
    }



}
