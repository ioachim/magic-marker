package magicmarker.scanner;

import sun.misc.Regexp;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ioachim
 * Date: 3/25/13
 * Time: 7:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class Token {

    public enum Type {

        TOKEN,
        ID,
        INT,
        TYPE,
        STRING,
        BOOLEAN
    }


    private static Pattern re = Pattern.compile("^[^:]+:([\\d\\s]+):([\\d\\s]+)\\s*(.*)$", Pattern.MULTILINE);
    private static Pattern valueRe = Pattern.compile("^([A-Za-z]+)<(.*)>$", Pattern.CASE_INSENSITIVE);

    private int line;
    private int column;
    private String token;
    private Type type;
    private String value;

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public int getLine() {
        return line;
    }

    public int getColumn() {
        return column;
    }

    public String getToken() {
        return token;
    }



    public Token(String token, int line, int column) {
        this.token = token;
        this.column = column;
        this.line = line;
        this.type = Type.TOKEN;
    }

    public static Token parse(String l) {
        Matcher match = re.matcher(l);

        ArrayList<Token> tokens = new ArrayList<Token>();

        if( match.matches() ) {

            int line = Integer.parseInt(match.group(1).trim());
            int column = Integer.parseInt(match.group(2).trim());
            String value = match.group(3).trim();

            Matcher valMatch = valueRe.matcher(value);

            Token token = new Token(value, line, column);

            if( valMatch.matches() ) {

                switch( valMatch.group(1).toLowerCase() ) {
                    case "string":
                        token.type = Type.STRING;
                        break;

                    case "id":
                    case "identifier":
                        token.type = Type.ID;
                        break;

                    case "int":
                        token.type = Type.INT;
                        break;

                    case "bool":
                    case "boolean":
                        token.type = Type.BOOLEAN;
                        break;

                    case "type":
                        token.type = Type.TYPE;
                        break;

                    default:
                        System.err.printf(String.format("Unknown value token type %s", valMatch.group(1)));
                        return null;
                }

                token.value = valMatch.group(2);

            }
            else {
                if( value.charAt(0) == '"' && value.charAt(value.length()-1) == '"') {
                    value = value.substring(1, value.length()-1);
                }

                if (value == "\\\"") {
                    value = "\"";
                }

                token.value = value.toLowerCase();
            }

            return token;


        }
        else {
            throw new IllegalArgumentException(String.format("Invalid token definition %s", l));
        }

    }

    public String toString() {

        if (type == Type.TOKEN ){
            return String.format("Token{ %d:%d %s }", line, column, token);
        }
        else {
            return String.format("Token{ %d:%d %s %s }", line, column, type, token);
        }
    }


    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Token))      {
            return false;
        }

        Token ex = this;
        Token out = (Token)obj;

            if( ex.getLine() != out.getLine()) {
                return false;
            }

        if( ex.getColumn() != out.getColumn()) {
            return false;
        }

        if( ex.getType() != out.getType()) {
            return false;
        }

        if( ex.getType() == Token.Type.STRING && !out.getValue().startsWith(ex.getValue())) {
            return false;
        }
        if( ex.getType() == Token.Type.BOOLEAN && !out.getValue().toLowerCase().equals(ex.getValue().toLowerCase())) {
            return false;
        }
        else if( !ex.getValue().equals(out.getValue())) {
            return false;
        }

        return true;
    }

    public void printDiff(Token other) {
        
        Token ex = this;
        Token out = other;
        
        if( ex.getLine() != out.getLine()) {
            System.err.printf("Error: expected line %d, got %d\n", ex.getLine(), out.getLine());
            return;
        }

        if( ex.getColumn() != out.getColumn()) {
            System.err.printf("Error: expected column %d, got %d\n", ex.getColumn(), out.getColumn());
            return;
        }

        if( ex.getType() != out.getType()) {
            System.err.printf("Error: expected type %s, got %s (%d:%d)\n", ex.getType(), out.getType(), ex.getLine(), ex.getColumn());
            return;
        }

        if( ex.getType() == Token.Type.STRING && !out.getValue().trim().replace(">", "").startsWith(ex.getValue().trim())) {
            System.err.printf("Error: expected %s, got %s (%d:%d)\n", ex.getValue(), out.getValue(), ex.getLine(), ex.getColumn());
            return;
        }
        else if( !ex.getValue().equals(out.getValue())) {
            System.err.printf("Error: expected %s, got %s (%d:%d)\n", ex.getValue(), out.getValue(), ex.getLine(), ex.getColumn());
            return;
        }

    }
}


