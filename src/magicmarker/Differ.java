package magicmarker;

import java.util.*;

public class Differ<T> {

    private List<T> _left;
    private List<T> _right;
    private int[][] C;

    public Differ(List<T> left, List<T> right) {
        _left = left;
        _right = right;
        LCSLength();
    }


    private void LCSLength() {
        C = new int[_left.size()+1][_right.size()+1];

        for ( int i = 0; i < _left.size(); i++) {
            C[i][0] = 0;
        }

        for ( int j = 0; j < _right.size(); j++) {
                C[0][j] = 0;
        }

        for ( int i = 1; i <= _left.size(); i++) {
            for ( int j = 1; j <= _right.size(); j++) {

                if( _left.get(i-1).equals(_right.get(j-1)) ){

                    C[i][j] = C[i-1][j-1] + 1;
                }
                else {
                    C[i][j] = Math.max(C[i][j-1], C[i-1][j]);
                }
            }
        }
    }

    public void printDiff() {
        printDiff(_left.size(), _right.size());
    }

    public void printDiff(int i, int j) {

        T l = i > 0 ? _left.get(i-1) : null;
        T r = j > 0 ? _right.get(j-1) : null;

        if( i > 0 && j > 0 && l.equals(r)) {
            printDiff(i-1, j-1);
            //System.err.printf("= %s\n", r);
        }
        else if (j > 0 && (i == 0 || (C[i][j-1] >= C[i-1][j]))) {
            printDiff( i, j-1);            
            System.err.printf("+ %s\n", r);

            //print "+ " + Y[j];
        }
        else if (i > 0 && (j == 0 || (C[i][j-1] < C[i-1][j]))) {
            printDiff(i-1, j);
            System.err.printf("- %s\n", l);
        }
    }

    public int length() {
        return C[_left.size()][_right.size()];
    }

}