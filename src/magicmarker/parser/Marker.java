package magicmarker.parser;

import magicmarker.Differ;
import java.util.*;
import java.io.*;

class Marker {

    public static void main(String[] args) {

        if(args.length < 1) {
            System.out.println("Usage java -cp magicmarker.jar magicmarker.parser.Marker <folder>");
            return;

        }
        File root = new File(args[0]);

        File[] sources = root.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".cl");
            }
        });


        double accumulatedMark = 0;
        int accumulatedSucceses = 0;

        for( File f : sources ) {
            System.err.println(f);
            try {
                Marker c = new Marker(f);
                double mark = c.process();
                accumulatedMark += mark;
                accumulatedSucceses += (int)mark;

            }
            catch(IOException e) {
                System.err.printf("Could not process file %s\n", f.getPath());
            }
        }

        double testCoverage = accumulatedSucceses/(double)sources.length;
        double successRatio = accumulatedMark/sources.length;


        System.err.printf("Test coverage: %d/%d = %.2f\n", accumulatedSucceses, sources.length, testCoverage);
        System.err.printf("Line coverage: %.2f%%", successRatio*100);

    }

    List<String> _expected;
    List<String> _output;


    public Marker(File f) throws IOException {
        File expected = new File(f.getPath() + ".parse");

        if( !expected.exists() ){
            System.err.printf(String.format("Could not find expected results file for %s\n", f));
            return;
        }

        ProcessBuilder pb = new ProcessBuilder("ant", String.format("-Dcool.source=%s", f.getPath()), "parse");
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }

        String line;
        StringBuilder antOutput = new StringBuilder();
        BufferedReader out = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while((line = out.readLine()) != null) {
            antOutput.append(line);
            antOutput.append("\n");
        }

        if( p.exitValue() != 0) {
            System.out.print(antOutput);
            System.exit(-1);
        }



        File output = new File(f.getPath() + ".out");

        if( !output.exists()){
            System.err.printf(String.format("Could not find output results file for %s\n", f));
            return;
        }

        BufferedReader reader = new BufferedReader(new FileReader(expected));
        _expected = readResults(reader);
        reader.close();

        reader = new BufferedReader(new FileReader(output));
        _output = readResults(reader);
        reader.close();
    }

    public double process() throws IOException {
        Differ<String> diff = new Differ<String>(_expected, _output);
        diff.printDiff();

        int matched = diff.length();
        int total = Math.max(_expected.size(), _output.size());
        
        System.err.printf("%d/%d -> %d%%\n", matched, total, 100*matched/total);
        return matched/(double)total;
    }

    private List<String> readResults(BufferedReader reader) throws IOException {
        String line;
        List<String> list = new ArrayList<String>();
        while((line = reader.readLine()) != null) {
            line = line.replace("\t", "    ");
            
            // ignore empty lines
            if( line.trim().equals("")) {
                continue;
            }

            list.add(line);
        }

        return list;
    }


}
